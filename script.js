const API_INICIAR_TESTE = '/api/iniciar-teste';
const API_STATUS_TESTE = '/api/status-teste';
const API_INICIAR_GRAVACAO = '/api/iniciar-gravacao';
const API_STATUS_GRAVACAO = '/api/status-gravacao';
const MIN_ID_LENGTH = 5;
const INVALID_ID_MESSAGE = 'ID inválido';

document.getElementById('idPlaca').focus();

document.getElementById('idPlaca').addEventListener('keydown', function(event) {
    if (event.key === 'Enter') {
        document.getElementById('startId').focus();
    }
});

document.getElementById('startId').addEventListener('keydown', function(event) {
    if (event.key === 'Enter') {
        document.getElementById('startButton').click();
    }
});

async function postData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return response.json();
}

function setStatus(elementId, status, message) {
    const element = document.getElementById(elementId);
    element.className = status.toLowerCase();
    element.innerText = message;
}

function validateInputs(idPlaca, startId) {
    if (idPlaca.length < MIN_ID_LENGTH || startId.length < MIN_ID_LENGTH) {
        setStatus('resultadoStatus', 'red', INVALID_ID_MESSAGE);
        setStatus('tensaoStatus', 'red', 'FALHA');
        setStatus('firmwareStatus', 'red', 'FALHA');
        resetForm();
        return false;
    }
    return true;
}

async function startTest() {
    const idPlaca = document.getElementById('idPlaca').value;
    const startId = document.getElementById('startId').value;

    if (!validateInputs(idPlaca, startId)) {
        return;
    }

    // Limpa o campo resultado antes de iniciar o teste
    setStatus('resultadoStatus', 'gray', '');

    document.getElementById('startButton').innerText = 'TESTANDO';
    document.getElementById('startButton').disabled = true;

    const initialResponse = await postData(API_INICIAR_TESTE, { idPlaca, startId });
    if (initialResponse.status === 'error') {
        setStatus('resultadoStatus', 'red', initialResponse.message);
        setStatus('tensaoStatus', 'red', 'FALHA');
        setStatus('firmwareStatus', 'red', 'FALHA');
        resetForm();
        return;
    }

    setStatus('tensaoStatus', 'yellow', 'TESTANDO');

    let tensaoCompleted = false;
    let firmwareCompleted = false;

    while (!tensaoCompleted) {
        const tensaoResponse = await postData(API_STATUS_TESTE, { idPlaca, startId });
        if (tensaoResponse.status === 'error') {
            setStatus('resultadoStatus', 'red', tensaoResponse.message);
            setStatus('tensaoStatus', 'red', 'FALHA');
            resetForm();
            return;
        } else if (tensaoResponse.status === 'success') {
            setStatus('tensaoStatus', 'green', 'OK');
            tensaoCompleted = true;
        }
        await new Promise(resolve => setTimeout(resolve, 5000));
    }

    const gravaResponse = await postData(API_INICIAR_GRAVACAO, { idPlaca, startId });
    if (gravaResponse.status === 'error') {
        setStatus('resultadoStatus', 'red', gravaResponse.message);
        setStatus('firmwareStatus', 'red', 'FALHA');
        resetForm();
        return;
    }

    setStatus('firmwareStatus', 'yellow', 'GRAVANDO');

    while (!firmwareCompleted) {
        const firmwareResponse = await postData(API_STATUS_GRAVACAO, { idPlaca, startId });
        if (firmwareResponse.status === 'error') {
            setStatus('resultadoStatus', 'red', firmwareResponse.message);
            setStatus('firmwareStatus', 'red', 'FALHA');
            resetForm();
            return;
        } else if (firmwareResponse.status === 'success') {
            setStatus('firmwareStatus', 'green', 'OK');
            firmwareCompleted = true;
        }
        await new Promise(resolve => setTimeout(resolve, 5000));
    }

    setStatus('resultadoStatus', 'green', 'Processo concluído com sucesso!');
    resetForm();
}

function resetForm() {
    document.getElementById('idPlaca').value = '';
    document.getElementById('startId').value = '';
    document.getElementById('idPlaca').focus();
    document.getElementById('startButton').innerText = 'COMEÇAR';
    document.getElementById('startButton').disabled = false;
    setStatus('tensaoStatus', 'gray', 'AGUARDANDO');
    setStatus('firmwareStatus', 'gray', 'AGUARDANDO');
}
