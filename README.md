# Interface de Teste de PCI

## Estrutura do Projeto

```
/projeto
  ├── index.html
  ├── style.css
  └── script.js
```

### Instruções para Executar

1. **Crie a Estrutura de Pastas**:
   - Crie uma pasta no seu computador, por exemplo, `JIGA_V1`.
   - Dentro dessa pasta, crie os arquivos `index.html`, `style.css` e `script.js` com o código fornecido.

2. **Execute o Projeto**:
   - Você pode abrir o arquivo `index.html` diretamente no seu navegador. Basta clicar duas vezes no arquivo ou clicar com o botão direito e selecionar "Abrir com" e escolher seu navegador preferido.

3. **Configuração da API Local**:
   - Certifique-se de que a API local esteja em execução e que responda às URLs conforme os exemplos de JSON fornecidos abaixo.

## Exemplos de JSON

### 1. Iniciar Teste

**POST /api/iniciar-teste**

#### Request:
```json
{
    "idPlaca": "12345",
    "startId": "67890"
}
```

#### Response (sucesso):
```json
{
    "status": "success"
}
```

#### Response (erro):
```json
{
    "status": "error",
    "message": "Erro ao iniciar teste"
}
```

### 2. Status do Teste

**POST /api/status-teste**

#### Request:
```json
{
    "idPlaca": "12345",
    "startId": "67890"
}
```

#### Response (sucesso):
```json
{
    "status": "success"
}
```

#### Response (em andamento):
```json
{
    "status": "running"
}
```

#### Response (erro):
```json
{
    "status": "error",
    "message": "Erro no teste de tensão"
}
```

### 3. Iniciar Gravação

**POST /api/iniciar-gravacao**

#### Request:
```json
{
    "idPlaca": "12345",
    "startId": "67890"
}
```

#### Response (sucesso):
```json
{
    "status": "success"
}
```

#### Response (erro):
```json
{
    "status": "error",
    "message": "Erro ao iniciar gravação"
}
```

### 4. Status da Gravação

**POST /api/status-gravacao**

#### Request:
```json
{
    "idPlaca": "12345",
    "startId": "67890"
}
```

#### Response (sucesso):
```json
{
    "status": "success"
}
```

#### Response (em andamento):
```json
{
    "status": "running"
}
```

#### Response (erro):
```json
{
    "status": "error",
    "message": "Erro na gravação do firmware"
}
```

### Validação de IDs

Para garantir que os IDs fornecidos sejam válidos, eles devem ter no mínimo n caracteres (definido como constante no código). Caso contrário, a mensagem "ID inválido" será exibida na seção "Resultado".

```json
{
    "status": "error",
    "message": "ID inválido"
}
```

## Fluxo da Aplicação

1. O usuário insere o ID da placa no campo "ID Placa".
2. Ao pressionar "Enter", o foco muda para o campo "Start ID".
3. O usuário insere o ID de início no campo "Start ID".
4. Ao pressionar "Enter", o botão "COMEÇAR" é acionado automaticamente.
5. A aplicação envia um POST para iniciar o teste.
6. Se a resposta for um erro, a mensagem de erro é exibida na seção "Resultado".
7. Se o teste é iniciado com sucesso, a seção "Teste de Tensão" exibe "TESTANDO".
8. A aplicação envia POSTs periódicos para verificar o status do teste.
9. Se o teste é concluído com sucesso, a seção "Teste de Tensão" exibe "OK".
10. A aplicação envia um POST para iniciar a gravação do firmware.
11. Se a gravação é iniciada com sucesso, a seção "Gravação de Firmware" exibe "GRAVANDO".
12. A aplicação envia POSTs periódicos para verificar o status da gravação.
13. Se a gravação é concluída com sucesso, a seção "Gravação de Firmware" exibe "OK".
14. A mensagem "Processo concluído com sucesso!" é exibida na seção "Resultado".
15. Se qualquer etapa falhar, a mensagem de erro correspondente é exibida e a aplicação retorna ao estado inicial.